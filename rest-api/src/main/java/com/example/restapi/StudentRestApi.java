package com.example.restapi;

import com.example.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentRestApi {
@Autowired
StudentRepository studentRepository;
    @GetMapping("/students")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(studentRepository.findAll());
    }
}
