package com.example.repository.Provider;

import org.apache.ibatis.jdbc.SQL;

public class StudentProvider {
    public String findAll(){
        return new SQL(){{
            SELECT("*");
            FROM("student");
        }}.toString();
    }
}
